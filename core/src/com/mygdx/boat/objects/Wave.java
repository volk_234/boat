package com.mygdx.boat.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Align;
import com.mygdx.boat.variables.Assets;
import com.mygdx.boat.variables.FixtureTypes;

public class Wave extends BaseObject {

    public Wave(World world, Vector2 position, float angle) {
        super(world, position, angle);
        body.setType(BodyDef.BodyType.KinematicBody);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.set(new float[]{0, 0, 2, 1, 2, 0});

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.friction = 1;

        body.createFixture(fixtureDef).setUserData(FixtureTypes.WAVE);
        polygonShape.dispose();

        body.setLinearVelocity(-10, 0);

        textureRegion = game.skin.getRegion(Assets.WAVE);
        setSize(3, 1.2f);
        setOrigin(Align.center);
    }
}

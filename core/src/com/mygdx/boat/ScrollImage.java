package com.mygdx.boat;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Александр on 21.11.2017.
 */

public class ScrollImage extends Actor {
    private TextureRegion texture;
    private float tX;
    private float speed;

    public ScrollImage(float speed, TextureRegion texture) {
        this.speed = speed;
        this.texture = texture;
        this.texture.getTexture().setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.ClampToEdge);
        this.texture.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        tX = 0;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        tX -= speed * delta;
        if (tX < -getWidth())
            tX = 0;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, tX, getY(), getWidth(), getHeight());
        batch.draw(texture, tX + getWidth(), getY(), getWidth(), getHeight());
    }
}

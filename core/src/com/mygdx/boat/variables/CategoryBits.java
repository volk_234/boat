package com.mygdx.boat.variables;

public class CategoryBits {
    public static short NONE = 0x0000;
    public static short DEFAULT = 0x0001;
    public static short CAR = 0x0002;
    public static short OBJECT = 0x0004;
    public static short GOAL = 0x0008;
}

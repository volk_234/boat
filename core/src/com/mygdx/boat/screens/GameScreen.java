package com.mygdx.boat.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.boat.BoatGame;
import com.mygdx.boat.BuoyancyController;
import com.mygdx.boat.ScrollImage;
import com.mygdx.boat.WorldContactListener;
import com.mygdx.boat.objects.Player;
import com.mygdx.boat.objects.Shark;
import com.mygdx.boat.objects.Wave;
import com.mygdx.boat.variables.Assets;
import com.mygdx.boat.variables.Constants;
import com.mygdx.boat.variables.FixtureTypes;

public class GameScreen implements Screen {
    private BoatGame game = (BoatGame) Gdx.app.getApplicationListener();
    private Skin skin = game.skin;
    private Stage stage, uiStage;
    private World world;
    private Box2DDebugRenderer box2DDebugRenderer;
    private Array<Body> deletingBodies = new Array<>();
    private Player player;
    private ScrollImage waterActor;

    BuoyancyController buoyancyController;

    public GameScreen() {
        stage = new Stage(new FillViewport(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT));
        uiStage = new Stage(new ScreenViewport());
        Table table = new Table();
        uiStage.addActor(table);
        table.setFillParent(true);

        box2DDebugRenderer = new Box2DDebugRenderer();

        uiStage.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                player.jump();
                return true;
            }
        });

//        stage.setDebugAll(true);
//        debugStage();

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(uiStage);
        start();
    }

    @Override
    public void render(float delta) {

        while (deletingBodies.size > 0) {
            Body body = deletingBodies.pop();
            world.destroyBody(body);
        }

        stage.act(delta);
        uiStage.act(delta);

        world.step(1 / 60f, 6, 3);
        buoyancyController.step();

        game.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        game.shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
                new Color(0xecf07bff),
                new Color(0xd2ab40ff),
                new Color(0x6c68f0ff),
                new Color(0x6885f0ff)
        );
        game.shapeRenderer.end();

        stage.draw();
        uiStage.draw();

        box2DDebugRenderer.render(world, stage.getCamera().combined);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        stage.clear();
    }

    @Override
    public void dispose() {

    }

    private void debugStage() {
        ((OrthographicCamera) stage.getCamera()).zoom = 3f;
        stage.getCamera().translate(-10, -10, 0);
        stage.getCamera().update();
    }

    private void start() {
        initWorld();
        initPlayer();
        stage.addAction(Actions.forever(Actions.sequence(
                Actions.delay(3),
                Actions.run(this::generateObject)
        )));
        waterActor.toFront();


    }

    public void beginContactWithFluid(Fixture fixture) {
        buoyancyController.addBody(fixture);
    }

    public void endContactWithFluid(Fixture fixture) {
        buoyancyController.removeBody(fixture);
    }

    private void initWorld() {
        world = new World(new Vector2(0, -10), false);
        world.setContactListener(new WorldContactListener(this));

        BodyDef waterBodyDef = new BodyDef();
        waterBodyDef.position.set(Constants.WORLD_WIDTH, Constants.WATER_HEIGHT * 0.5f);
        waterBodyDef.type = BodyDef.BodyType.StaticBody;
        Body waterBody = world.createBody(waterBodyDef);

        PolygonShape waterShape = new PolygonShape();
        waterShape.setAsBox(Constants.WORLD_WIDTH, Constants.WATER_HEIGHT * 0.5f);
        waterBody.createFixture(waterShape, 1).setUserData(FixtureTypes.WATER);
        waterBody.getFixtureList().first().setSensor(true);
        waterShape.dispose();

        buoyancyController = new BuoyancyController(world, waterBody.getFixtureList().first());


        BodyDef destroyZoneBodyDef = new BodyDef();
        destroyZoneBodyDef.type = BodyDef.BodyType.DynamicBody;
        destroyZoneBodyDef.gravityScale = 0;
        destroyZoneBodyDef.position.x = -Constants.WORLD_WIDTH * 0.4f;
        Body destroyZoneBody = world.createBody(destroyZoneBodyDef);

        PolygonShape destroyZoneShape = new PolygonShape();
        destroyZoneShape.setAsBox(Constants.WORLD_WIDTH * 0.2f, Constants.WORLD_HEIGHT);

        FixtureDef destroyZoneFixtureDef = new FixtureDef();
        destroyZoneFixtureDef.shape = destroyZoneShape;
        destroyZoneFixtureDef.isSensor = true;
        destroyZoneBody.createFixture(destroyZoneFixtureDef).setUserData(FixtureTypes.DESTROY_ZONE);
        destroyZoneShape.dispose();


        waterActor = new ScrollImage(5f, skin.getRegion(Assets.WATER));
        waterActor.setSize(Constants.WORLD_WIDTH, Constants.WATER_HEIGHT);
        stage.addActor(waterActor);
    }

    private void initPlayer() {
        player = new Player(world, Constants.PLAYER_SPAWN_POSITION, 0);
        stage.addActor(player);
    }

    public void deleteBody(Body body) {
        deletingBodies.add(body);
    }

    public void lose() {
        game.toMenu();
    }

    private void generateObject() {
        Actor actor;
        switch (MathUtils.random(0, 1)) {
            case 0:
                actor = new Wave(world, new Vector2(Constants.WORLD_WIDTH * 1.3f, Constants.WATER_HEIGHT), 0);
                break;
            case 1:
                actor = new Shark(world, new Vector2(Constants.WORLD_WIDTH * 1.3f, Constants.WATER_HEIGHT * 0.9f), 0);
                break;
            default:
                actor = new Wave(world, new Vector2(Constants.WORLD_WIDTH * 1.3f, Constants.WATER_HEIGHT), 0);
                break;
        }
        stage.addActor(actor);
        waterActor.toFront();
    }
}

package com.mygdx.boat.variables;


public enum FixtureTypes {
    PLAYER, PLAYER_FORWARD_SENSOR, PLAYER_TOP_SENSOR, WATER, SHARK, DESTROY_ZONE, WAVE
}

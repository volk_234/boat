package com.mygdx.boat;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.boat.screens.GameScreen;
import com.mygdx.boat.screens.MenuScreen;

public class BoatGame extends Game {

    public MenuScreen menuScreen;
    public GameScreen gameScreen;
    public MyAssetManager assetManager;
    public MySkin skin;
    public ShapeRenderer shapeRenderer;


    @Override
    public void create() {
        shapeRenderer = new ShapeRenderer();
        assetManager = new MyAssetManager();
        skin = new MySkin(assetManager);
        menuScreen = new MenuScreen();
        gameScreen = new GameScreen();

        toMenu();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        super.render();
    }

    @Override
    public void dispose() {
    }

    public void toMenu() {
        setScreen(menuScreen);
    }

    public void toGame() {
        setScreen(gameScreen);
    }
}


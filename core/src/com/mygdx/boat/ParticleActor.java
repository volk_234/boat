package com.mygdx.boat;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Александр on 05.03.2018.
 */

public class ParticleActor extends Actor {
    private ParticleEffect particleEffect;

    public ParticleActor(ParticleEffect particleEffect) {
        super();
        this.particleEffect = particleEffect;
        this.particleEffect.start();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        particleEffect.setPosition(getX(), getY());
        particleEffect.update(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        particleEffect.draw(batch);
    }
}

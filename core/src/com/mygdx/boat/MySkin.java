package com.mygdx.boat;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.boat.variables.Assets;


public class MySkin extends Skin {

    public MySkin(AssetManager assetManager) {
        super(assetManager.get(Assets.ATLAS, TextureAtlas.class));

        add("default", assetManager.get("default.ttf", BitmapFont.class));

        Label.LabelStyle labelDefaultStyle = new Label.LabelStyle(getFont("default"), Color.BLUE);
        add("default", labelDefaultStyle);
    }
}

package com.mygdx.boat;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.mygdx.boat.objects.Player;
import com.mygdx.boat.objects.Shark;
import com.mygdx.boat.objects.Wave;
import com.mygdx.boat.screens.GameScreen;
import com.mygdx.boat.variables.FixtureTypes;

import java.util.Objects;

public class WorldContactListener implements ContactListener {
    private GameScreen gameScreen;

    public WorldContactListener(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
    }

    @Override
    public void beginContact(Contact contact) {
        if (isContactOf(contact, FixtureTypes.PLAYER, FixtureTypes.WATER)) {
            Objects.requireNonNull(getBodyWithType(contact, Player.class)).setJumpAbility(true);
            gameScreen.beginContactWithFluid(getFixture(contact, FixtureTypes.PLAYER));
        } else if (isContactOf(contact, FixtureTypes.PLAYER, FixtureTypes.WAVE)) {
            Objects.requireNonNull(getBodyWithType(contact, Player.class)).setJumpAbility(true);
        } else if (isContactOf(contact, FixtureTypes.PLAYER_FORWARD_SENSOR, FixtureTypes.SHARK)) {
            gameScreen.lose();
        } else if (isContactOf(contact, FixtureTypes.PLAYER_TOP_SENSOR, FixtureTypes.WATER)) {
            gameScreen.lose();
        } else if (isContactOf(contact, FixtureTypes.SHARK, FixtureTypes.DESTROY_ZONE)) {
            Objects.requireNonNull(getBodyWithType(contact, Shark.class)).destroy();
        } else if (isContactOf(contact, FixtureTypes.SHARK, FixtureTypes.WATER)) {
            gameScreen.beginContactWithFluid(getFixture(contact, FixtureTypes.SHARK));
        } else if (isContactOf(contact, FixtureTypes.WAVE, FixtureTypes.WATER)) {
            gameScreen.beginContactWithFluid(getFixture(contact, FixtureTypes.WAVE));
        } else if (isContactOf(contact, FixtureTypes.WAVE, FixtureTypes.DESTROY_ZONE)) {
            Objects.requireNonNull(getBodyWithType(contact, Wave.class)).destroy();
        }
    }

    @Override
    public void endContact(Contact contact) {
        if (isContactOf(contact, FixtureTypes.PLAYER, FixtureTypes.WATER)) {
            Objects.requireNonNull(getBodyWithType(contact, Player.class)).setJumpAbility(false);
            gameScreen.endContactWithFluid(getFixture(contact, FixtureTypes.PLAYER));
        } else if (isContactOf(contact, FixtureTypes.PLAYER, FixtureTypes.WAVE)) {
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    private boolean isContactOf(Contact contact, FixtureTypes a, FixtureTypes b) {
        return ((contact.getFixtureA().getUserData() == a && contact.getFixtureB().getUserData() == b) ||
                (contact.getFixtureA().getUserData() == b && contact.getFixtureB().getUserData() == a));
    }

    private <T> T getBodyWithType(Contact contact, Class<T> clazz) {
        if (contact.getFixtureA().getBody().getUserData() != null &&
                contact.getFixtureA().getBody().getUserData().getClass().isAssignableFrom(clazz)) {
            return (clazz.cast(contact.getFixtureA().getBody().getUserData()));
        } else if (contact.getFixtureB().getBody().getUserData() != null &&
                contact.getFixtureB().getBody().getUserData().getClass().isAssignableFrom(clazz)) {
            return (clazz.cast(contact.getFixtureB().getBody().getUserData()));
        }
        return null;
    }

    private Fixture getFixture(Contact contact, FixtureTypes fixtureType) {
        if (contact.getFixtureA().getUserData() == fixtureType) {
            return contact.getFixtureA();
        } else if (contact.getFixtureB().getUserData() == fixtureType) {
            return contact.getFixtureB();
        } else {
            return null;
        }
    }
}

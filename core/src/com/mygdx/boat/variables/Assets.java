package com.mygdx.boat.variables;

public class Assets {
    public static String ATLAS = "atlas.atlas";
    public static String FONT = "font.ttf";

    public static String PLAYER = "skeleton-walk";
    public static String SHARK = "shark";
    public static String WATER = "water";
    public static String WAVE = "wave";
    public static String BG = "bg";
    public static String PARTICLE = "particle.p";
}

package com.mygdx.boat.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Align;
import com.mygdx.boat.variables.Assets;
import com.mygdx.boat.variables.Constants;
import com.mygdx.boat.variables.FixtureTypes;

public class Shark extends BaseObject {

    public Shark(World world, Vector2 position, float angle) {
        super(world, position, angle);
        body.setType(BodyDef.BodyType.DynamicBody);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(Constants.SHARK_WIDTH * 0.5f, Constants.SHARK_HEIGHT * 0.5f);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.friction = 0;
        fixtureDef.density = 0.8f;

        body.createFixture(fixtureDef).setUserData(FixtureTypes.SHARK);
        polygonShape.dispose();


        textureRegion = game.skin.getRegion(Assets.SHARK);
        setSize(Constants.SHARK_WIDTH, Constants.SHARK_HEIGHT);
        setOrigin(Align.center);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        body.applyForce(new Vector2(-15,0), body.getPosition(), true);
    }
}

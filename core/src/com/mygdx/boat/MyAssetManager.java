package com.mygdx.boat;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.mygdx.boat.variables.Assets;
import com.mygdx.boat.variables.Constants;

public class MyAssetManager extends AssetManager {
    public MyAssetManager() {
        super();
        load(Assets.ATLAS, TextureAtlas.class);

        FileHandleResolver resolver = new InternalFileHandleResolver();
        setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));
        setLoader(BitmapFont.class, ".otf", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter freeTypeFontParameter =
                new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        freeTypeFontParameter.fontFileName = Assets.FONT;
        freeTypeFontParameter.fontParameters.characters = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя" +
                "abcdefghijklmnopqrstuvwxyzАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯABCDEFGHIJKLMNOPQR" +
                "STUVWXYZ0123456789][_!$%#@|/?-+=()*&.;:,{}´`'<>;";
        freeTypeFontParameter.fontParameters.size = (int) (Constants.H1 * 0.5f);
        load("default.ttf", BitmapFont.class, freeTypeFontParameter);
        finishLoading();
    }
}
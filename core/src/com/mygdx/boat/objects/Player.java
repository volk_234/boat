package com.mygdx.boat.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Align;
import com.mygdx.boat.ParticleActor;
import com.mygdx.boat.variables.Assets;
import com.mygdx.boat.variables.Constants;
import com.mygdx.boat.variables.FixtureTypes;

public class Player extends BaseObject {
    private boolean jumpAbility = true;
    private ParticleActor particleActor;
    private Animation animation;
    private float stateTime = 3000;

    public Player(World world, Vector2 position, float angle) {
        super(world, position, angle);
        body.setType(BodyDef.BodyType.DynamicBody);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.set(new float[]{
                -Constants.PLAYER_WIDTH * 0.1f, -Constants.PLAYER_HEIGHT * 0.5f,
                -Constants.PLAYER_WIDTH * 0.5f, Constants.PLAYER_HEIGHT * 0.5f,
                Constants.PLAYER_WIDTH * 0.5f, Constants.PLAYER_HEIGHT * 0.5f,
                Constants.PLAYER_WIDTH * 0.1f, -Constants.PLAYER_HEIGHT * 0.5f,
        });

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.friction = 0.1f;
        fixtureDef.density = 0.2f;

        body.createFixture(fixtureDef).setUserData(FixtureTypes.PLAYER);
        polygonShape.dispose();

        FixtureDef fixtureSensorDef = new FixtureDef();
        polygonShape = new PolygonShape();
        polygonShape.set(new float[]{
                Constants.PLAYER_WIDTH * 0.4f, Constants.PLAYER_HEIGHT * -0.6f,
                Constants.PLAYER_WIDTH * 0.4f, 0,
                Constants.PLAYER_WIDTH * 0.6f, 0,
                Constants.PLAYER_WIDTH * 0.6f, Constants.PLAYER_HEIGHT * -0.6f,
        });
        fixtureSensorDef.density = 0;
        fixtureSensorDef.shape = polygonShape;
        fixtureSensorDef.isSensor = true;
        body.createFixture(fixtureSensorDef).setUserData(FixtureTypes.PLAYER_FORWARD_SENSOR);

        FixtureDef fixtureTopSensorDef = new FixtureDef();
        polygonShape = new PolygonShape();
        polygonShape.setAsBox(Constants.PLAYER_WIDTH * 0.1f, Constants.PLAYER_HEIGHT * 0.05f,
                new Vector2(0, Constants.PLAYER_HEIGHT * 0.7f), 0f);
        fixtureTopSensorDef.shape = polygonShape;
        fixtureTopSensorDef.isSensor = true;
        fixtureTopSensorDef.density = 0;
        body.createFixture(fixtureTopSensorDef).setUserData(FixtureTypes.PLAYER_TOP_SENSOR);

        setSize(Constants.PLAYER_WIDTH, Constants.PLAYER_WIDTH);
        setOrigin(Align.center);

        ParticleEffect particleEffect = new ParticleEffect();
        particleEffect.load(Gdx.files.internal(Assets.PARTICLE), game.skin.getAtlas());
        particleEffect.scaleEffect(1f / Constants.PPT);
        particleActor = new ParticleActor(particleEffect);
        addActor(particleActor);
        particleActor.setPosition(getX(Align.right), Constants.PLAYER_HEIGHT*1.3f);
        particleActor.setVisible(false);


        animation = new Animation<>(1 / 30f,
                game.skin.getRegions(Assets.PLAYER), Animation.PlayMode.NORMAL);
    }

    @Override
    public void act(float delta) {
        stateTime += delta;
        body.setTransform(Constants.PLAYER_SPAWN_POSITION.x, body.getPosition().y, body.getAngle());
        super.act(delta);

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        TextureAtlas.AtlasRegion frame = (TextureAtlas.AtlasRegion) animation.getKeyFrame(stateTime);
        float dim = getHeight() / frame.getRegionHeight();
        float width = frame.getRegionWidth() * dim;
        batch.draw(frame,
                getX() + getWidth() * 0.5f - width * 0.5f,
                getY()*1.2f, getOriginX(), getOriginY(), width, getHeight(), 1, 1, getRotation());
    }

    public void jump() {
        if (jumpAbility) {
            body.applyLinearImpulse(Constants.PLAYER_JUMP_VECTOR, body.getWorldCenter(), true);
            body.applyAngularImpulse(0.15f, true);
            stateTime = 0;
        }
    }

    public void setJumpAbility(boolean value) {
        jumpAbility = value;
        particleActor.setVisible(value);
    }
}

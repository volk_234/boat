package com.mygdx.boat.variables;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class Constants {
    public static float W1 = Gdx.graphics.getWidth() * 0.1f;
    public static float H1 = Gdx.graphics.getHeight() * 0.1f;
    public static float WORLD_WIDTH = 32;
    public static final int PPT = (int) ((float) Gdx.graphics.getWidth() / WORLD_WIDTH);
    public static float WORLD_HEIGHT = Gdx.graphics.getHeight() / PPT;

    public static float WATER_HEIGHT = WORLD_HEIGHT * 0.3f;

    public static float PLAYER_WIDTH = 4;
    public static float PLAYER_HEIGHT = 1;
    public static Vector2 PLAYER_SPAWN_POSITION = new Vector2(5, 8);
    public static Vector2 PLAYER_JUMP_VECTOR = new Vector2(0, 3);

    public static float SHARK_WIDTH = 2;
    public static float SHARK_HEIGHT = 1;
}

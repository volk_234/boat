package com.mygdx.boat.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Align;
import com.mygdx.boat.BoatGame;

public class BaseObject extends Group {
    BoatGame game = (BoatGame) Gdx.app.getApplicationListener();

    protected Body body;
    protected TextureRegion textureRegion;

    BaseObject(World world, Vector2 position, float angle) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(position);
        bodyDef.angle = angle;
        bodyDef.fixedRotation = false;
        body = world.createBody(bodyDef);
        body.setUserData(this);
    }

    void updatePosition() {
        setPosition(body.getPosition().x, body.getPosition().y, Align.center);
        setRotation(body.getAngle() * MathUtils.radiansToDegrees);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        updatePosition();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color temp = batch.getColor();
        if (textureRegion != null) {
            batch.setColor(getColor());
            batch.draw(textureRegion, getX(), getY(), getOriginX(), getOriginY(),
                    getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
            batch.setColor(temp);
        }
        super.draw(batch, parentAlpha);
    }

    public Body getBody() {
        return body;
    }

    public void destroy() {
        game.gameScreen.deleteBody(body);
        this.remove();
    }
}
